import React from "react";
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Sidebar from "./components/Sidebar";
import About from "./pages/About";
import Dashboard from "./pages/Dashboard";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Sidebar>
          <Routes>
            <Route path='/' extact="true" element={<Dashboard />} />
            <Route path='/about' element={<About />} />
          </Routes>
        </Sidebar>
      </div>
    </BrowserRouter>
  );
}

export default App;
