import React, { useState } from "react";
import { FaBars, FaTh, FaUserAlt } from 'react-icons/fa';
import { NavLink } from "react-router-dom";
import '../App.css';

const Sidebar = ({ children }) => {
    const menuItem = [
        {
            path: "/",
            name: "Dashboard",
            icon: <FaTh />,
        },
        {
            path: "/about",
            name: "About",
            icon: <FaUserAlt />,
        },
    ];

    const [isOpen, setIsOpen] = useState(true);
    const toggle = () => setIsOpen(!isOpen);

    return (
        <div className="container-fluid">
            <div style={{ width: isOpen ? "280px" : "55px" }} className="sidebar">
                <div className="top_section">
                    <h2 style={{ display: isOpen ? "flex" : "none" }} className="logo">
                        Welcome
                    </h2>
                    <div style={{ marginLeft: isOpen ? "70px" : "-25px" }} className="bars">
                        <FaBars onClick={toggle} />
                    </div>
                </div>

                {
                    menuItem.map((item, index) => (
                        <NavLink to={item.path} key={index} className="link"                    >
                            <div className="icon">{item.icon}</div>
                            <div style={{ display: isOpen ? "flex" : "none" }} className="link_text">{item.name}</div>
                        </NavLink>
                    ))
                }
            </div>
            <main>{children}</main>
        </div>
    );
}

export default Sidebar;
